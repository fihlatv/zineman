import re
from unidecode import unidecode
from flask import redirect, url_for, request
from flask_admin import Admin, AdminIndexView, BaseView, expose
from flask_admin.contrib.sqla import ModelView
from flask_user import current_user
from wtforms import TextAreaField, PasswordField
from wtforms.fields.html5 import IntegerField
from wtforms.widgets import TextArea

from zineman.models import Role, User, Page, Volume, Subscription, session


def _create_slug(title):
    """
    Convert the title to a slug
    """
    return re.sub(r'\W+', '-', unidecode(title).lower()).strip('-')


class CKTextAreaWidget(TextArea):
    def __call__(self, field, **kwargs):
        if kwargs.get('class'):
            kwargs['class'] += ' ckeditor'
        else:
            kwargs.setdefault('class', 'ckeditor')
        return super(CKTextAreaWidget, self).__call__(field, **kwargs)


class CKTextAreaField(TextAreaField):
    widget = CKTextAreaWidget()


class AuthorizedMixin(object):
    def is_accessible(self):
        return current_user.is_active and current_user.is_authenticated \
            and current_user.has_role('superuser')

    def inaccessible_callback(self, name, **kwargs):
        if current_user.is_authenticated:
            return redirect(url_for('/'))
        else:
            return redirect(url_for('user.login', next=request.url))


class AuthorizedAdminIndexView(AuthorizedMixin, AdminIndexView):
    pass


class AuthorizedModelView(AuthorizedMixin, ModelView):
    extra_js = ['//cdn.ckeditor.com/4.6.0/standard/ckeditor.js']
    column_exclude_list = ('password',)
    column_descriptions = {
        'weight': 'Use this to order items in the menu'
    }
    form_excluded_columns = ('slug',)
    form_overrides = {
        'body': CKTextAreaField,
        'password': PasswordField,
        'weight': IntegerField
    }

    def on_model_change(self, form, model, is_create):
        if isinstance(model, Page):
            model.slug = _create_slug(model.title)


class SettingsView(AuthorizedMixin, BaseView):
    @expose('/')
    def index(self):
        return self.render('settings.html')


admin = Admin(name='ZineMan', template_mode='bootstrap4', index_view=AuthorizedAdminIndexView())
admin.add_view(AuthorizedModelView(Volume, session, name='Volumes'))
admin.add_view(AuthorizedModelView(User, session, name='Users'))
admin.add_view(AuthorizedModelView(Role, session, name='Roles'))
admin.add_view(AuthorizedModelView(Subscription, session, name='Subscriptions'))
admin.add_view(AuthorizedModelView(Page, session, name='Pages'))
admin.add_view(SettingsView(name='Settings', endpoint='settings'))
