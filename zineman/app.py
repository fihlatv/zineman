from datetime import datetime

from flask import Flask
from flask_admin.menu import MenuLink
from flask_user import UserManager

from zineman.admin import admin
from zineman.config import config_from_file
from zineman.models import User, Role, db, get_or_create
from zineman.views.main import main


def _setup_db(app):
    # Create the tables
    db.create_all()
    # Create the roles
    subscriber_role = get_or_create(db.session, Role, name='subscriber')
    superuser_role = get_or_create(db.session, Role, name='superuser')
    # Optionally create a superuser
    if app.config.get('ZINEMAN_SUPERUSER', None):
        superuser = app.config['ZINEMAN_SUPERUSER']
        if not User.query.filter(User.email == superuser['email']).first():
            user = User(
                name=superuser.get('name', 'Superuser'),
                email=superuser['email'],
                email_confirmed_at=datetime.utcnow(),
                password=app.user_manager.hash_password(superuser.get('password', 'Password1')),
                active=True
            )
            user.roles.extend([subscriber_role, superuser_role])
            db.session.add(user)
    db.session.commit()


def create_app():
    app = Flask('zineman')
    config_from_file(app, 'zineman.conf')
    db.init_app(app)
    admin.init_app(app)
    UserManager(app, db, User)
    # Register blueprints
    app.register_blueprint(main)
    with app.app_context():
        _setup_db(app)
        # Set up menu shortcuts
        admin.add_link(MenuLink('Back to main site', '/'))
        admin.add_link(MenuLink('Logout', '/user/sign-out'))
    return app
