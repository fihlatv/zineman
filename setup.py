import re

from setuptools import find_packages, setup


with open('README.rst', 'rt', encoding='utf8') as readme_file:
    README = readme_file.read()
with open('zineman/__init__.py', 'rt', encoding='utf8') as zineman_file:
    VERSION = re.search(r'__version__ = \'(.*?)\'', zineman_file.read()).group(1)


setup(
    name='ZineMan',
    version=VERSION,
    author='Raoul Snyman',
    description='A simple online magazine manager for providing restricted access to online volumes for subscribers',
    long_description=README,
    long_description_content_type='text/x-rst',
    url='https://gitlab.com/superfly/zineman',
    project_urls={
        'Documentation': 'https://superfly.gitlab.io/zineman',
        'Code': 'https://gitlab.com/superfly/zineman',
        'Issue tracker': 'https://gitlab.com/superfly/zineman/issues'
    },
    license='GPLv3+',
    packages=find_packages(),
    platforms='any',
    python_requires='>=3.5',
    install_requires=[
        'Flask',
        'Flask-Admin',
        'Flask-SQLAlchemy',
        'Flask-User',
        'pyyaml',
        'unidecode',
    ],
    extras_require={
        'dev': [
            'pytest>=3',
            'pytest-cov',
        ],
    },
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Environment :: Web Environment',
        'Framework :: Flask',
        'Intended Audience :: Other Audience',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3 :: Only',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content :: Content Management System',
        'Topic :: Internet :: WWW/HTTP :: WSGI',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application',
    ],
)
