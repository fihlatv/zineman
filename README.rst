ZineMan
=======

ZineMan is a simple online magazine manager. It is designed to give your subscribers restricted access to your
online publication in PDF format.


Installing
----------

Install and update using `pip`_:

.. code-block:: text

   pip install -U ZineMan


Set up
------

Set up ZineMan by creating a configuration file like ``zineman.conf``:

.. code-block:: ini

   [flask]
   secret_key = <create a secret for sessions etc>

   [sqlalchemy]
   database_uri = sqlite:///zineman.sqlite

   [zineman]
   title = My Awesome Magazine


Links
-----

* Website: https://gitlab.com/superfly/zineman
* Documentation: https://superfly.gitlab.io/zineman
* License: https://gitlab.com/superfly/zineman/blob/master/LICENSE
* Issue tracker: https://gitlab.com/superfly/zineman/issues
